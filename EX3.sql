set autoprint on;
set serveroutput on;

create or replace view ddl_info as
select id_student, name_student, year, semester, course_id, sec_id, title, day,
    start_hr, start_min, end_hr, end_min, room_number, name_instructor, dn_student as dept_name
from section
join (select id as id_instructor, name as name_instructor, course_id from (teaches join instructor using (id)))
using (course_id)
join (select id as id_student, name as name_student, dept_name as dn_student, course_id from (takes join student using (id)))
using (course_id)
join course using (course_id)
join time_slot using (time_slot_id);

-- type object
drop type type_ex3 FORCE;
create or replace type type_ex3 as object (
    t_key varchar2(20),
    t_value varchar2(50)
);

-- table
--drop type table_ex3 FORCE;
create type table_ex3 as table of type_ex3;

create or replace procedure SP_LOC_DU_LIEU_EX3 (
    p_table table_ex3, p_cursor out sys_refcursor)
is
    v_sql varchar2(1000);
    v_result number := 0;
begin
    v_sql := 'select * from ddl_info where ' || p_table(1).t_key || '=''' || p_table(1).t_value || ''' and '
            || p_table(2).t_key || '=''' || p_table(2).t_value || '''';
--    execute immediate v_sql into v_result;
    open p_cursor for v_sql;
exception 
    when others then
        dbms_output.PUT_LINE('===> ERROR: ' || SQLERRM);
end;
/

var rc refcursor;
declare
    filter_1 type_ex3;
    filter_2 type_ex3;
    custom_table table_ex3;
begin
    filter_1 := type_ex3('dept_name', 'Physics');
    filter_2 := type_ex3('year', 2010);
    custom_table := table_ex3(filter_1, filter_2);
    SP_LOC_DU_LIEU_EX3(custom_table, :rc);
end;

--    for idx in custom_table.first .. custom_table.last
--    loop
--        dbms_output.PUT_LINE(idx || ': ' || custom_table(idx).t_key || '-' || custom_table(idx).t_value);
--    end loop;