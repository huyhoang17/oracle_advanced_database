create or replace function func_convert_grade_point(grade varchar2)
return number
is
    grade_point number := 1;
begin
    grade_point := case grade 
             when 'A ' then 4
             when 'A+' then 4
             when 'A-' then 3.5
             when 'B+' then 3.5
             when 'B ' then 3
             when 'B-' then 2.5
             when 'C+' then 2.5
             when 'C ' then 2
             when 'C-' then 1
             else 1 end;
    return grade_point;
exception
    when others then
        dbms_output.put_line('ERRRRRRR: ' || SQLERRM);
end;

-- INDEX
drop index take_index;
drop index student_index;
drop index advisor_index;
create index take_index on takes (grade);
create index student_index on student ( name, dept_name, tot_cred);
create index advisor_index on advisor (i_id);

var rc refcursor;
exec SP_LOC_DU_LIEU('dept_name', 'Physics', :rc);
