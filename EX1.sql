set serveroutput on
set autoprint on
create or replace function func_convert_grade_point(grade varchar2)
return number
is
    grade_point number := 1;
begin
    grade_point := case grade 
             when 'A ' then 4
             when 'A+' then 4
             when 'A-' then 3.5
             when 'B+' then 3.5
             when 'B ' then 3
             when 'B-' then 2.5
             when 'C+' then 2.5
             when 'C ' then 2
             when 'C-' then 1
             else 1 end;
    return grade_point;
exception
    when others then
        dbms_output.put_line('ERRRRRRR: ' || SQLERRM);
end;

create or replace function check_course_point_to_pass (
    course_point number)
return number 
as
    l_result number := 0;
begin
    if course_point > 1 then
        l_result := 1;
    end if;
    return l_result;
end;

create or replace procedure SP_SHOW_RESULT_STUDENT (p_student_id number)
is
    student_id number;
    student_name varchar2(32767);
    avg_point number := 0.0;
    no_credits_passed number := 0;
    no_credits_to_pass number := 128;
begin

    select id, name into student_id, student_name
    from student where id=p_student_id;

    select round(sum(max(func_convert_grade_point(t.grade)) * c.credits) / sum(c.credits), 4) into avg_point
    from takes t, course c
    where id=p_student_id and t.course_id = c.course_id
    group by t.course_id, c.credits;

    select sum(credits) into no_credits_passed from (select c.credits
    from takes t join course c on t.course_id = c.course_id
    where t.id=p_student_id
    group by t.course_id, c.credits
    having max(func_convert_grade_point(t.grade)) > 1
    order by t.course_id);

    if avg_point > 1 and no_credits_passed >= no_credits_to_pass then
        dbms_output.PUT_LINE('===> So tin chi tich luy: ' || no_credits_passed);
        dbms_output.PUT_LINE('===> Du dieu kien tot nghiep: ' || student_id || '-' || student_name || '-' || avg_point);
    else
        dbms_output.PUT_LINE('===> So tin chi tich luy: ' || no_credits_passed);
        dbms_output.PUT_LINE('===> KHONG du dieu kien tot nghiep: ' || student_id || '-' || student_name || '-' || avg_point);
    end if;

exception 
    when others then
        dbms_output.PUT_LINE('===> ERROR: ' || SQLERRM);
end;
/

exec SP_SHOW_RESULT_STUDENT(63288);
