set serveroutput on
set autoprint on
--  Mã sinh viên, Họ tên sinh viên, Năm học, Kỳ học, Khóa học, Thời gian học, Phòng học, Giảng viên, Khoa viện.
create or replace view ddl_info as
select id_student, name_student, year, semester, course_id, sec_id, title, day,
    start_hr, start_min, end_hr, end_min, room_number, name_instructor, dn_student as dept_name
from section
join (select id as id_instructor, name as name_instructor, course_id from (teaches join instructor using (id)))
using (course_id)
join (select id as id_student, name as name_student, dept_name as dn_student, course_id from (takes join student using (id)))
using (course_id)
join course using (course_id)
join time_slot using (time_slot_id);

create or replace procedure SP_LOC_DU_LIEU (
    p_field VARCHAR, p_value VARCHAR, p_cursor out sys_refcursor)
is
    v_sql varchar2(1000);
begin
    v_sql := 'select * from ddl_info where ' || p_field || '=''' || p_value || '''';
    dbms_output.PUT_LINE('===> v_sql: ' || v_sql);
    open p_cursor for v_sql;
exception 
    when others then
        dbms_output.PUT_LINE('===> ERROR: ' || SQLERRM);
end;
/

var rc refcursor;
exec SP_LOC_DU_LIEU('dept_name', 'Physics', :rc);