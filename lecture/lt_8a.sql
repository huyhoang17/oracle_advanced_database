-- grant access
create role manager_test;

grant create table, create view to manager_test;

create user test_user identified by password123;

drop user test_user;

grant manager_test to test_user;

--

select department_id, job_id, sum(salary)
from employees
where department_id < 60
group by rollup (department_id, job_id);

select department_id, job_id, sum(salary)
from employees
where department_id < 60
group by cube (department_id, job_id);

-- grouping funcion
select department_id deptid, job_id job, sum(salary), grouping(department_id) grp_dept, grouping(job_id) grp_job
from employees
where department_id < 50
group by rollup (department_id, job_id);

-- grouping set
select department_id, job_id, manager_id, avg(salary)
from employees 
group by grouping sets ((department_id, job_id), (job_id, manager_id));

-- retrieve data using subquery
select last_name, salary, department_id
from employees outer
where salary > (
    select avg(salary) from employees
    where department_id = outer.department_id);

-- exists
select employee_id, last_name, job_id, department_id
from employees outer
where exists (
    select 'X' from employees
    where manager_id = outer.employee_id);

select employee_id, last_name, job_id, department_id


