select count(*) from employees;

-- create view
create view empvu80_ct as
select employee_id, last_name, salary
from employees where department_id = 80;

desc empvu80_ct;

-- create view by using column alias
create view salvu50_ct as
select employee_id ID_NUMBER, last_name NAME, salary * 12 ANN_SALARY
from employees where department_id = 50;

desc salvu50_ct;

select * from salvu50_ct;

-- creating a complex view
create view dept_sum_vu_ct (name, minsal, maxsal, avgsal) as
select d.department_name, MIN(e.salary), MAX(e.salary), AVG(e.salary)
from employees e, departments d
where e.department_id = d.department_id
group by d.department_name;

select * from dept_sum_vu_ct;

-- option: with check option
create or replace view empvu50_ct as
select * from employees
where department_id = 20
with check option constraint empvu50_ck;

select * from empvu50_ct;

create or replace view empvu10_ct (employee_number, employee_name, job_title)
as select employee_id, last_name, job_id
from employees where department_id = 10
with read only;

select * from empvu10_ct;

-- drop a view
drop view empvu80_ct;

-- PRACTICE
create or replace view employees_vu as 
select employee_id, last_name employee, department_id
from employees;

desc employees;

select * from employees_vu;

select employee, department_id from employees_vu;
select count(*) from employees_vu;

-- create sequences
create sequence dept_deptid_seq
increment by 10
start with 120
maxvalue 9999
nocache
nocycle;

-- dept_deptid_seq.NEXTVAL

insert into departments (department_id, department_name, location_id)
values (dept_deptid_seq.NEXTVAL, 'Support', 2500);

select MAX(department_id) from departments;

-- alter sequence
alter sequence dept_deptid_seq
increment by 20
maxvalue 999999
nocache
nocycle;

-- drop sequence
drop sequence dept_deptid_seq;

-- create synonyms
create synonym d_sum for dept_sum_vu_ct;
create synonym dep for departments;

select * from departments sample(1);
select * from employees sample(1);
-- HOMEWORK 1
create or replace view dept50 (empno, employee, deptno) as 
select employee_id, last_name, department_id from employees
where department_id = 50
with read only;

select * from dept50 where employee='Fripp';
--select * from dept50;
--select * from employees;
update employees 
set
    department_id=80
where last_name='Fripp';

update dept50 
set
    deptno=80
where employee='Fripp';

-- create another dept50 view
create or replace view dept50_2 (empno, employee, deptno) as 
select employee_id, last_name, department_id from employees
where department_id = 50
with check option constraint dept50_ck;

select * from dept50_2 where employee='Jones';
--select * from dept50_2;
update dept50_2
set
    deptno=80
where employee='Jones';

update employees 
set
    department_id=80
where last_name='Jones';

-- HOMEWORD 2
create sequence DEPT_ID_SEQ
increment by 10
start with 200
maxvalue 1000
nocache
nocycle;

select ID, NAME from dept_ct;

insert into dept_ct (id, name)
values 
    (DEPT_ID_SEQ.NEXTVAL, 'Education');

insert into dept_ct (id, name)
values
    (DEPT_ID_SEQ.NEXTVAL, 'Administration');

--insert all
--into dept_ct (id, name) value (DEPT_ID_SEQ.NEXTVAL, 'Education')
--into dept_ct (id, name) value (DEPT_ID_SEQ.NEXTVAL, 'Administration');

-- HOMEWORD 3
create synonym emp for employees;