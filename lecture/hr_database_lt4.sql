--- Practice 1
select * from employees sample(1);

select last_name , job_id, salary as Sal from employees;

select employee_id, last_name, (salary * 12) as "ANNUAL SALARY" from employees;

select distinct job_id from employees;

select last_name || ', ' || job_id as "Employee and Title" from employees;

-- Practice 2
select last_name || ', ' || salary as "Employee and Monthly Salary" from employees
where salary between 5000 and 12000 
and department_id in (20, 50);

select last_name, salary, commission_pct from employees
where commission_pct is not null
order by salary desc, commission_pct desc;

select last_name from employees
where last_name like '%a%'
and last_name like '%e%';

select e.last_name, j.job_title, e.salary from employees e, jobs j
where j.job_title in ('SA_REP', 'ST_CLERK')
and e.salary not in (2500, 3500, 7000);