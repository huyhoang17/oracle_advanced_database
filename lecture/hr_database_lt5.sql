select last_name, (SYSDATE - hire_date) / 7 as WEEK
from employees 
where department_id = 90;

select last_name, TO_CHAR(hire_date, 'DD Month YYYY') as HIREDATE
from employees;

select to_char(salary, '$99,999.00') salary
from employees
where last_name = 'Ernst';

-- coalesce: manager_id <-- commission_pct
select last_name, coalesce(manager_id, commission_pct, -1) commm
from employees
order by commission_pct;

select last_name, manager_id, commission_pct
from employees
order by commission_pct

-- conditional expression: case, decode
select last_name, job_id, salary,
case job_id when 'IT_PROG' then 1.10 * salary
            when 'ST_CLERK' then 1.15 * salary
            when 'SA_REP' then 1.20 * salary
else salary end "REVISED_SALARY"
from employees;

select last_name, job_id, salary,
decode (job_id, 'IT_PROG', 1.10 * salary,
                'ST_CLERK', 1.15 * salary,
                'SA_REP', 1.20 * salary,
                salary)
revised_salary
from employees;

select last_name, salary,
decode (trunc(salary/2000, 0),
            0, 0.00,
            1, 0.09,
            2, 0.20,
            3, 0.30,
            4, 0.40,
            5, 0.42,
            6, 0.44,
            0.45) tax_rate
from employees
where department_id = 80;

-- HOMEWORK 1
select initcap(last_name) cap_last_name, length(last_name) len_last_name
from employees
where substr(first_name, 1, 1) in ('J', 'A', 'M')
order by last_name;

-- HOMEWORK 2
select last_name, ROUND(MONTHS_BETWEEN(SYSDATE, hire_date)) MONTHS_WORKED
from employees
order by MONTHS_WORKED;

-- HOMEWORK 3
select last_name, to_char(round(add_months(hire_date, 6), 'DY') + 1, 'DAY, "the" ddspth "of" MONTH, YYYY') REVIEW, salary 
from employees;

-- HOMEWORK 4
select last_name, NVL(to_char(commission_pct), 'No Commission') COMM
from employees;

-- HOMEWORK 5
select
decode (job_id, 'AD_PRES', 'A',
                'ST_MAN', 'B',
                'IT_PROG', 'C',
                'SA_REP', 'D',
                'ST_CLERK', 'E',
                0) JOB_GRADE
from employees;

--select to_char(add_months(hire_date, 6), 'DAY MONTH YYYY') from employees;

--select * from employees;

-- Enter a prompt
select initcap(last_name) "Name", length(last_name) "Length"
from employees
where last_name like (initcap('&var%'))
order by last_name;

-- group function
select avg(salary), max(salary), min(salary), sum(salary)
from employees
where job_id like '%REP%';

-- HOMEWORD 2.1
select round(max(salary)) MAXIMUM, round(min(salary)) MINIMUM, round(sum(salary)) SUM, round(avg(salary)) AVERAGE
from employees;

-- HOMEWORD 2.2
select round(max(salary)) MAXIMUM, round(min(salary)) MINIMUM, round(sum(salary)) SUM, round(avg(salary)) AVERAGE, job_id
from employees
group by job_id;

-- HOMEWORD 2.3
select count(distinct manager_id) "Number of Managers"
from employees;

-- HOMEWORD 2.4
select manager_id from employees
where manager_id is not null
group by manager_id
having min(salary) > 6000;

select salary from employees where manager_id in (
select manager_id from employees
where manager_id is not null
group by manager_id
having min(salary) > 6000);
