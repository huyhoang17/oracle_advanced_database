-- subquery
select employee_id, manager_id, department_id
from employees
where (manager_id, department_id) in (
    select manager_id, department_id
    from employees where employee_id in (199, 174))
and employee_id not in (199, 174);

select employee_id, last_name,
(case 
    when deparment_id = (select department_id from departments where location_id = 1800)
    then 'Canada' else 'Usa' end
) location
from employees;