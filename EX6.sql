set serveroutput on
set autoprint on

create or replace function func_convert_grade_point(grade varchar2)
return number
is
    grade_point number := 1;
begin
    grade_point := case grade 
             when 'A ' then 4
             when 'A+' then 4
             when 'A-' then 3.5
             when 'B+' then 3.5
             when 'B ' then 3
             when 'B-' then 2.5
             when 'C+' then 2.5
             when 'C ' then 2
             when 'C-' then 1
             else 1 end;
    return grade_point;
exception
    when others then
        dbms_output.put_line('ERRRRRRR: ' || SQLERRM);
end;

begin
    dbms_output.put_line(func_convert_grade_point('B '));
end;

-- count = 30k
--drop view view_semester_year_info;
create or replace view view_semester_year_info as
select s.semester, s.year, s.course_id, t.id, t.grade from (select s.semester, s.year, s.course_id from section s
group by s.semester, s.year, s.course_id) s, takes t
where s.course_id = t.course_id and s.semester = t.semester and s.year = t.year
order by t.year, t.semester;

-- filter course of single student that course_id existed more than 1
select id, course_id from takes group by course_id, id having count(course_id) > 1 order by course_id;

-- no filter
select sum(c1.credits * func_convert_grade_point(t1.grade)) / sum(c1.credits)
            from takes t1, course c1
            where t1.id = 10527 and t1.course_id = c1.course_id and t1.year <= 2010;

-- with filter
select sum(c1.credits * max(func_convert_grade_point(t1.grade))) / sum(c1.credits)
            from takes t1, course c1
            where t1.id = 10527 and t1.course_id = c1.course_id and t1.year <= 2010
            group by t1.course_id, c1.credits;


create or replace function func_convert_semester(year_ number, semester varchar2)
return number
is
    year_number varchar2(5);
begin
    if semester = 'Spring' then
        year_number := concat(year_, 1);
    else
        year_number := concat(year_, 2);
    end if;
    return to_number(year_number);
end;

create or replace function func_cal_total_credits_ex6(p_student_id number, year_ number, semester varchar2)
return number
is
    total_credits number := 0;
    temp_semester number := 0;  -- NOTE: assignment before checking!
begin
    dbms_output.put_line(func_convert_semester(year_, semester));
    temp_semester := func_convert_semester(year_, semester);
    select sum(credits) into total_credits from (select c2.credits
            from takes t2, course c2
            where t2.id=p_student_id and t2.course_id = c2.course_id 
                and func_convert_semester(t2.year, t2.semester) <= temp_semester
            group by t2.course_id, c2.credits
            having max(func_convert_grade_point(t2.grade)) > 1);
    return total_credits;
exception
    when others then
        dbms_output.put_line('ERRRRRRR: ' || SQLERRM);
end;

create or replace procedure SP_SHOW_RESULT_STUDENT_V2 (p_student_id number, p_cursor out sys_refcursor)
is
begin
    open p_cursor for
    select t.id, s.name, t.year, t.semester
        , round(sum(func_convert_grade_point(t.grade) * c.credits) / sum(c.credits), 2) as "Diem TB hoc ki"
        , round((select sum(max(func_convert_grade_point(t1.grade)) * c1.credits) / sum(c1.credits)
            from takes t1, course c1
            where t1.id=p_student_id and t1.course_id = c1.course_id
                and func_convert_semester(t1.year, t1.semester) <= func_convert_semester(t.year, t.semester)
            group by t1.course_id, c1.credits), 2) as "Diem TB tich luy"
        , (func_cal_total_credits_ex6(p_student_id, t.year, t.semester)) as "So tin chi tich luy"
        from student s join takes t on s.id=t.id, course c
        where t.id=p_student_id and c.course_id=t.course_id
        group by t.id, s.name, t.year, t.semester
        order by t.year, t.semester desc;
end;
/

var rc refcursor;
--exec SP_SHOW_RESULT_STUDENT_V2(63288, :rc);
--exec SP_SHOW_RESULT_STUDENT_V2(24746, :rc);
exec SP_SHOW_RESULT_STUDENT_V2(123, :rc);