set serveroutput on
set autoprint on

create or replace function func_convert_level_student(no_credits number)
return varchar2
is
    grade_point varchar2(20) := 'SV nam nhat';
begin
    grade_point := case 
             when no_credits < 32 then 'SV nam nhat'
             when 32 <= no_credits and no_credits < 64 then 'SV nam hai'
             when 64 <= no_credits and no_credits < 96 then 'SV nam ba'
             when 96 <= no_credits and no_credits < 128 then 'SV nam bon'
             when no_credits >= 128 then 'SV nam nam'
             else 'SV nam nhat' end;
    return grade_point;
exception
    when others then
        dbms_output.put_line('ERRRRRRR: ' || SQLERRM);
end;

create or replace function func_cal_total_credits(p_student_id number, year_ number)
return number
is
    total_credits number := 0;
begin
    select sum(credits) into total_credits from (select c2.credits
            from takes t2, course c2
            where t2.id=p_student_id and t2.course_id = c2.course_id and t2.year <= year_
            group by t2.course_id, c2.credits
            having max(func_convert_grade_point(t2.grade)) > 1);
    return total_credits;
exception
    when others then
        dbms_output.put_line('ERRRRRRR: ' || SQLERRM);
end;

create or replace function func_convert_student_capacity(avg_credit_point number)
return varchar2
is
    avg_point number := 0;
    grade_point varchar2(20) := 'Kém';
begin
    avg_point := round(avg_credit_point, 2);
    grade_point := case 
             when 3.6 <= avg_point then 'Xuat sac'
             when 3.2 <= avg_point and avg_point < 3.6 then 'Gioi'
             when 2.5 <= avg_point and avg_point < 3.2 then 'Kha'
             when 2.0 <= avg_point and avg_point < 2.5 then 'Trung Binh'
             when 1.0 <= avg_point and avg_point < 2.0 then 'Yeu'
             when avg_point < 1.0 then 'Kem'
             else 'Kem' end;
    return grade_point;
exception
    when others then
        dbms_output.put_line('ERRRRRRR: ' || SQLERRM);
end;

begin
    dbms_output.put_line(func_convert_level_student(100));
    dbms_output.put_line(func_convert_student_capacity(2.49));
end;

select id, course_id from takes
where id=63288
group by course_id, id having count(course_id) > 1 order by course_id;

select sum(max(func_convert_grade_point(t.grade)) * c.credits) / sum(c.credits) as "AVG_POINT"
            from takes t, course c
            where t.id=63288 and t.course_id = c.course_id and t.year <= 2010 and t.semester >= 'Fall'
            group by t.course_id, c.credits;
            
-- filter theo so tin chi qua cua tung ki
select sum(c.credits)
from takes t, course c
where t.id=63288 and t.course_id = c.course_id and t.year <= 2010 and t.semester >= 'Fall'
group by t.course_id, c.credits --having count(t.course_id) > 1
having max(func_convert_grade_point(t.grade)) > 1;  -- filter course that have credit point <= 1


create or replace procedure SP_SHOW_RESULT_STUDENT_EX7 (p_student_id number, p_cursor out sys_refcursor)
is
begin
    open p_cursor for
    select t.id, s.name, t.year
        , func_convert_student_capacity(
            round((select sum(max(func_convert_grade_point(t1.grade)) * c1.credits) / sum(c1.credits)
            from takes t1, course c1
            where t1.id=p_student_id and t1.course_id = c1.course_id and t1.year <= t.year
            group by t1.course_id, c1.credits), 2)
        ) as "Xep loai"
        , func_convert_level_student(func_cal_total_credits(p_student_id, t.year)) as "Xep hang"        
--        , func_cal_total_credits(p_student_id, t.year) as "Xep hang"
        from student s join takes t on s.id=t.id, course c
        where t.id=p_student_id and c.course_id=t.course_id
        group by t.id, s.name, t.year
        order by t.year;
end;
/

var rc refcursor;
exec SP_SHOW_RESULT_STUDENT_EX7(63288, :rc);