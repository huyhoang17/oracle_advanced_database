drop function check_sal;
create or replace function check_sal (empno employees.employee_id%type)
return boolean 
is
    dept_id employees.department_id%type;
--    empno employees.employee_id%type;
    sal employees.salary%type;
    avg_sal employees.salary%type;
begin
    select salary, department_id
    into sal, dept_id
    from employees
    where employee_id = empno;
    
    select avg(salary)
    into avg_sal
    from employees
    where department_id = dept_id;
    
    if sal > avg_sal then
        return true;
    else
        return false;
    end if;
    
exception 
    when no_data_found then
        return null;
end;
/

set verify off;
set autoprint on;
set serveroutput on;
begin
    if (check_sal(205) is null) then
        dbms_output.put_line('The function returned Null due to exception');
    elsif (check_sal(205)) then
        dbms_output.put_line('salary > avg');
    else
        dbms_output.put_line('salary < avg');
    end if;
end;
/


create or replace procedure query_emp
    ( id in employees.employee_id%type,
      name out employees.last_name%type,
      salary out employees.salary%type)
is
begin
    select last_name, salary
    into name, salary
    from employees
    where employee_id = id;
end query_emp;

declare 
    emp_name employees.last_name%type;
    emp_sal employees.salary%type;
begin
    query_emp(171, emp_name, emp_sal);
end;


-- function
create or replace function get_sal
    (id employees.employee_id%type)
    return number
is
    sal employees.salary%type := 0;
begin
    select salary into sal
    from employees
    where employee_id=id;
    
    return sal;
end get_sal;

execute dbms_output.put_line(get_sal(100));

declare
    v_return number := 0;
begin
    v_return := get_sal(100);
    dbms_output.put_line(v_return);
end;

variable salary number;
exec :salary := get_sal(100);

select job_id, get_sal(employee_id) sal
from employees;

drop function tax;
create or replace function tax(value in number)
return number 
is
begin
    return (value * 0.08);
end tax;

select employee_id, last_name, salary, tax(salary) tax
from employees
where department_id=100;



-- trigger
create or replace trigger secure_emp
before insert on employees begin
if (to_char(sysdate, 'DY') in ('SAT', 'SUN')) or
   (to_char(sysdate, 'HH24:MI') not between '08:00' and '18:00') then
raise_application_error(-20500, 'You may insert' || ' into EMPLOYEES table only during ' || ' business hours.');
end if;
end;

insert into employees(employee_id, last_name, first_name, email, hire_date, job_id, salary, department_id)
values (300, 'Smith', 'Rob', 'RSMITH', sysdate, 'IT_PROG', 4500, 60);

create or replace trigger secure_emp_advance
before insert or update or delete on employees begin
if (to_char(sysdate, 'DY') in ('SAT', 'SUN')) or
   (to_char(sysdate, 'HH24:MI') not between '08:00' and '18:00') then
   if deleting then raise_application_error(-20502, 'can not del');
   elsif inserting then raise_application_error(-20500, 'can not insert');
   elsif updating('salary') then raise_application_error(-20503, 'can not update');
   else raise_application_error(-20504, 'can not update employees');
   end if;
end if;
end;

-- row trigger
create or replace trigger restrict_salary
before insert or update of salary on employees
for each row
begin
    if not (:new.job_id in ('AD_PRES', 'AD_VP')) and
            :new.salary > 15000 then
        raise_application_error(-20202, 'can not earn more');
    end if;
end;

-- trigger with old/new qialifier

create or replace trigger audit_emp_values
after delete or insert or update on employees
for each row
begin
    insert into audit_emp(user_name, time_stamp, id, old_last_name, new_last_name, old_title, new_title, old_salary, new_salary)
    values (USER, SYSDATE, :old.employee_id, :old.last_name, :new.last_name, :old.job_id, :new.job_id, :old.salary, :new.salary);
end;


-- advance trigger
create table log_trig_table (
    user_id number,
    log_date date,
    action varchar2(20)
);

create or replace trigger logon_trig
after logon on schema
begin 
    insert into log_trig_table(user_id, log_date, action)
    values (user, sysdate, 'Logging on');
end;

create or replace trigger logoff_trig
before logoff on schema
begin
    insert into log_trig_table(user_id, log_date, action)
    values (user, sysdate, 'Logging off');
end;