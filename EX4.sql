set serveroutput on
set autoprint on

select * from course
where title = 'Mobile Computing';

select * from prereq
where course_id = 603 or prereq_id = 603;

create or replace procedure prereq_course (
    course_name varchar2 default 'Mobile Computing')
as
    l_prereq_id number;
    temp_course_id number;
begin
    for course in (select * from course where title = course_name)
    loop
        temp_course_id := course.course_id;
        dbms_output.PUT_LINE('course_id: ' || temp_course_id);
        << inner_loop >>
        loop
            select count(prereq_id) into l_prereq_id
            from prereq
            where course_id = temp_course_id;
        
            if l_prereq_id = 0 then
                l_prereq_id := null;
                exit;
            end if;
            
            select prereq_id into l_prereq_id
            from prereq
            where course_id = temp_course_id;
            temp_course_id := l_prereq_id;
            dbms_output.PUT_LINE('===> Result: ' || temp_course_id);
                
        end loop inner_loop;
    end loop;
    
exception 
    when others then
        dbms_output.PUT_LINE('===> ERROR: ' || SQLERRM);
end;
/

exec prereq_course('Mobile Computing');
exec prereq_course('Marine Mammals');
--declare
--    l_result number;
--begin
--    select prereq_id into l_result from prereq where course_id = 408;
--    exception 
--        when NO_DATA_FOUND then
--            l_result := null;
--    dbms_output.PUT_LINE('===> ERROR: ' || l_result);
--    if l_result is null then
--        dbms_output.put_line('aaa');
--    end if;
--end;
